﻿STATE_429 = {
    id = 429
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1A4267" "x2B50AB" "x3505A1" "x363D4E" "x39E9BC" "x5ACC0F" "xB1500E" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 52
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 3308 #Hokhos Sea
}

STATE_430 = {
    id = 430
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0F5DA3" "x1E598E" "x97429C" "x9F8945" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 56
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}

STATE_431 = {
    id = 431
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x05C4AE" "x223E7E" "x6CE4EF" "x7AD8FD" "xA5478F" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_432 = {
    id = 432
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x53BF4D" "x5CEA19" "x8A7D0F" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 37
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}

STATE_433 = {
    id = 433
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2B2A60" "x80D80D" "xC565B4" "xEB0D15" "xFDADCD" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 56
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}

STATE_434 = {
    id = 434
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x18827F" "x23A0CB" "x293F4F" "x617C58" "x8A81E9" "xA004EE" "xAFD467" "xEEDF3F" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 28
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_435 = {
    id = 435
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x3BF953" "x437918" "x66CE66" "x840CA3" "x90C989" "xBA1779" "xEB52FC" "xF153C1" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 10
    arable_resources = { bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_436 = {
    id = 436
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x05A7F5" "x371111" "x373678" "x8EAF5B" "xB22330" "xB2ABD9" "xB52E11" "xD74A11" "xDC17E4" "xED21C9" "xFDBBB7" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 68
    arable_resources = { bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_437 = {
    id = 437
    subsistence_building = "building_subsistence_farms"
    provinces = { "x249618" "x33EEDA" "x5EEEAB" "x7C3AF6" "x8D5B32" "xAC3287" "xB66448" "xD4F36F" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 104
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_438 = {
    id = 438
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5CE45C" "x781DF0" "x86FC9F" "x991FC5" "xB67790" "xD3428A" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 89
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3306 #Odheongu Sea
}

STATE_439 = {
    id = 439
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1E1AE9" "xA24873" "xA776DD" "xADE1FA" "xB1AF47" "xD70DD7" "xE8B438" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 237
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3306 #Odheongu Sea
}

STATE_440 = {
    id = 440
    subsistence_building = "building_subsistence_farms"
    provinces = { "x296855" "x40BEA1" "x6A87F4" "x6A9C7B" "x87DAA1" "x88CDCC" "x8FB1A9" "xC4CA7A" "xD10D86" "xD70114" "xD8B8D1" "xFBEE01" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 132
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3306 #Odheongu Sea
}

STATE_441 = {
    id = 441
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x4D3279" "x640179" "x90B940" }
    impassable = { "x640179" "x4D3279" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 11
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}


