﻿COUNTRIES = {
	c:L06 = {
		effect_starting_technology_baashidi_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_no_police
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slave_trade
		activate_law = law_type:law_poor_laws

	}
}