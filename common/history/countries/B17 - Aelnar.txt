﻿COUNTRIES = {
	c:B17 = {
		effect_starting_technology_tier_3_tech = yes
		effect_starting_politics_reactionary = yes
		activate_law = law_type:law_magocracy #rule of Lithiel/Elissa
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_dark_arts_embraced

		set_institution_investment_level = {
			institution = institution_police
			level = 2
		}
	}
}