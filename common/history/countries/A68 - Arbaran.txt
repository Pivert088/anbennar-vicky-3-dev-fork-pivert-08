﻿COUNTRIES = {
	c:A68 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats #Elected Bureaucrats not possible without elections
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_free_trade	#enforced by powers so Anbennar keeps open
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_local_police
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_artifice_encouraged
		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged

		ig:ig_industrialists = {
			add_ruling_interest_group = yes
		}

		add_modifier = {
			name = demilitarized_zone
			months = 36500
		}
	}
}