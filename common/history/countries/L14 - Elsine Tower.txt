﻿COUNTRIES = {
	c:L14 = {
		effect_starting_technology_baashidi_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_magocracy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_traditional_magic_encouraged
		activate_law = law_type:law_no_police
	}
}