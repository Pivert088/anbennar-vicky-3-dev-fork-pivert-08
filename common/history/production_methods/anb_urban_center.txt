﻿# Anbennar - Removed Vanilla tag references + added our own
PRODUCTION_METHODS = {
	c:A01 = { # Anbennar
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
		#All Anb Subjects in the empire
		every_subject_or_below = {
			limit = {
				is_subject_type = puppet
			}
			activate_production_method = {
				building_type = building_urban_center
				production_method = pm_market_squares
			}
			activate_production_method = {
				building_type = building_urban_center
				production_method = pm_gas_streetlights
			}
		}
	}
	c:A03 = { # Lorent
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A04 = { # Gawed
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A06 = { # Gnomish Hiearchy
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A07 = { # Reveria
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A10 = { # Grombar
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
	}
	c:A18 = { # Bayvek
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A19 = { # Vertesk
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B19 = { # Cestirmark
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B20 = { # Marlliande
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B21 = { # Ynnsmouth
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B22 = { # Zanlib
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B23 = { # Isobelin
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_free_urban_clergy
		}
	}
	c:B24 = { # Thilvis
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B25 = { # Valorpoint
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:B89 = { # Daxwell
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
}
