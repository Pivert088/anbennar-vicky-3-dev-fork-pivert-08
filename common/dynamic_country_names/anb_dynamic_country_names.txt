﻿A49 = {	#dunno why this is a thing, Carneter is also Lorentish
	dynamic_country_name = {
		name = dyn_c_tretun
		adjective = dyn_c_tretun_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:A03
			#If you're a subject of Lorent
			is_subject_of = c:A03
		}
	}
}

A68 = {	#Arbaran Demilitarized Zone
	dynamic_country_name = {
		name = dyn_c_arbaran_demilitarized_zone
		adjective = dyn_c_arbaran_demilitarized_zone_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			c:A68 = { has_modifier = demilitarized_zone }
			exists = c:A01
			c:A01 = {
				has_diplomatic_pact = {
					who = c:A68
					type = puppet
					is_initiator = yes
				}
			}
		}
	}
}

B05 = {
	dynamic_country_name = {
		name = dyn_c_vanbury_guild
		adjective = dyn_c_vanbury_guild_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				has_government_type = gov_guild
			}
		}
	}
}
B21 = {
	dynamic_country_name = {
		name = dyn_c_posveagal
		adjective = dyn_c_posveagal_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				OR = {
					country_has_state_religion = rel:ynn_river_worship
					top_overlord = {
						country_has_state_religion = rel:ynn_river_worship
					}
					top_overlord = {
						any_primary_culture = {
							has_discrimination_trait = ynnic_heritage
						}
					}
				}
			}
		}
	}
}
B29 = {
	dynamic_country_name = {
		name = dyn_c_sarda
		adjective = dyn_c_sarda_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				NOT = {
					country_tier = hegemony
					country_tier = empire
				}
			}
		}
	}
}
B30 = {
	dynamic_country_name = {
		name = dyn_c_chippengard
		adjective = dyn_c_chippengard_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				NOT = {
					country_has_state_religion = rel:ynn_river_worship
					top_overlord = {
						country_has_state_religion = rel:ynn_river_worship
					}
					top_overlord = {
						any_primary_culture = {
							has_discrimination_trait = ynnic_heritage
						}
					}
				}
			}
		}
	}
}
B31 = {
	dynamic_country_name = {
		name = dyn_c_veykodirzag
		adjective = dyn_c_veykodirzag_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				is_subject = no
			}
		}
	}
}
B32 = {
	dynamic_country_name = {
		name = dyn_c_west_tipney
		adjective = dyn_c_west_tipney_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				NOT = {
					country_has_state_religion = rel:ynn_river_worship
					top_overlord = {
						country_has_state_religion = rel:ynn_river_worship
					}
					top_overlord = {
						any_primary_culture = {
							has_discrimination_trait = ynnic_heritage
						}
					}
				}
			}
		}
	}
}
B33 = {
	dynamic_country_name = {
		name = dyn_c_corinsfield
		adjective = dyn_c_corinsfield_adj
		
		is_main_tag_only = yes
		priority = 0
		
		trigger = {
			exists = scope:actor
			scope:actor = {
				NOT = {
					country_has_state_religion = rel:ynn_river_worship
					top_overlord = {
						country_has_state_religion = rel:ynn_river_worship
					}
					top_overlord = {
						any_primary_culture = {
							has_discrimination_trait = ynnic_heritage
						}
					}
				}
			}
		}
	}
}