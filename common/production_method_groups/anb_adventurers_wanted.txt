﻿

pmg_party_composition_building_adventurers_wanted = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_local_adventuring_parties_building_adventurers_wanted
		pm_hire_adventurer_companies_building_adventurers_wanted
		pm_military_operation_building_adventurers_wanted
	}
}


pmg_combat_party_composition_building_adventurers_wanted = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_local_adventuring_parties_building_adventurers_wanted
		pm_hire_adventurer_companies_building_adventurers_wanted
		pm_military_operation_building_adventurers_wanted
	}
}

pmg_intrigue_party_composition_building_adventurers_wanted = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_local_adventuring_parties_building_adventurers_wanted
		pm_hire_adventurer_companies_building_adventurers_wanted
		pm_internal_agents_building_adventurers_wanted
	}
}

pmg_dungeon_party_composition_building_adventurers_wanted = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_local_adventuring_parties_building_adventurers_wanted
		pm_hire_adventurer_companies_building_adventurers_wanted
		pm_excavation_crew_building_adventurers_wanted
	}
}



pmg_loot_share_building_adventurers_wanted = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_unregulated_loot_building_adventurers_wanted
		pm_regulated_distribution_building_adventurers_wanted
		pm_government_red_tape_building_adventurers_wanted
	}
}


#Types

pmg_base_building_adventurers_wanted = {
	production_methods = {
		default_building_adventurers_wanted
	}
}

pmg_dungeon_ancient_catacombs_building_adventurers_wanted = {
	production_methods = {
		combat_ancient_catacombs_building_adventurers_wanted
	}
}

pmg_combat_dragon_building_adventurers_wanted = {
	production_methods = {
		combat_dragon_building_adventurers_wanted
	}
}