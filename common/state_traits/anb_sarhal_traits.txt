﻿state_trait_deep_ardimyan_desert = {	
    icon = "gfx/interface/icons/state_trait_icons/dry_climate.dds"
	
	modifier = {
		state_construction_mult = -0.25
		state_infrastructure_mult = -0.25
		building_group_bg_agriculture_throughput_mult = -0.2
		state_mortality_mult = 0.15
	}
}

state_trait_alnamaliu_estuary = {	
    icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = 0.10
		state_infrastructure_add = 10
	}
}